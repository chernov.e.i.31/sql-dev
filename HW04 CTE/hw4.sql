/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "03 - Подзапросы, CTE, временные таблицы".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- Для всех заданий, где возможно, сделайте два варианта запросов:
--  1) через вложенный запрос
--  2) через WITH (для производных таблиц)
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Выберите сотрудников (Application.People), которые являются продажниками (IsSalesPerson), 
и не сделали ни одной продажи 04 июля 2015 года. 
Вывести ИД сотрудника и его полное имя. 
Продажи смотреть в таблице Sales.Invoices.
*/

select p.PersonID, p.FullName
from Application.People p
where p.IsSalesperson = 1
	and p.PersonID not in (
		select distinct i.SalespersonPersonID
		from Sales.Invoices i
		where i.InvoiceDate = '20150704') 

/*
2. Выберите товары с минимальной ценой (подзапросом). Сделайте два варианта подзапроса. 
Вывести: ИД товара, наименование товара, цена.
*/
select i.StockItemID, i.StockItemName, i.UnitPrice 
from Warehouse.StockItems i
where i.UnitPrice = (select MIN( UnitPrice) from Warehouse.StockItems)

declare @minPrice decimal(10,8) = (select MIN( UnitPrice) from Warehouse.StockItems)
;with Items as (select i.StockItemID, i.StockItemName, i.UnitPrice from Warehouse.StockItems i)

select StockItemID, StockItemName, UnitPrice
from Items 
where UnitPrice = @minPrice
/*
3. Выберите информацию по клиентам, которые перевели компании пять максимальных платежей 
из Sales.CustomerTransactions. 
Представьте несколько способов (в том числе с CTE). 
*/

select p.PersonID, p.FullName
from Application.People p
where p.PersonID in (
		select  top 5 SalespersonPersonID
		from Sales.CustomerTransactions t
			join Sales.Invoices i on i.InvoiceID = t.InvoiceID
		order by TransactionAmount desc)

;with topPersons as (
	select  top 5 SalespersonPersonID
	from Sales.CustomerTransactions t
		join Sales.Invoices i on i.InvoiceID = t.InvoiceID
	order by TransactionAmount desc)

select p.PersonID, p.FullName
from Application.People p
where p.PersonID in (select SalespersonPersonID from topPersons)
/*
4. Выберите города (ид и название), в которые были доставлены товары, 
входящие в тройку самых дорогих товаров, а также имя сотрудника, 
который осуществлял упаковку заказов (PackedByPersonID).
*/

;with topItemsId as (select top 3 i.StockItemID from Warehouse.StockItems i order by UnitPrice desc)
, orderInfo as (select distinct c.DeliveryCityID, i.PackedByPersonID
	from Sales.Orders o
		join Sales.Invoices i on i.OrderID = o.OrderID
		join Sales.OrderLines ol on ol.OrderID = o.OrderID
			and ol.StockItemID in (select StockItemID from topItemsId )
		join Sales.Customers c on c.CustomerID = o.CustomerID)

select ct.CityID, ct.CityName, p.FullName 
from Application.Cities ct 
	join (select  DeliveryCityID,PackedByPersonID from orderInfo) o on o.DeliveryCityID = ct.CityID
	join Application.People p on p.PersonID = o.PackedByPersonID

-- ---------------------------------------------------------------------------
-- Опциональное задание
-- ---------------------------------------------------------------------------
-- Можно двигаться как в сторону улучшения читабельности запроса, 
-- так и в сторону упрощения плана\ускорения. 
-- Сравнить производительность запросов можно через SET STATISTICS IO, TIME ON. 
-- Если знакомы с планами запросов, то используйте их (тогда к решению также приложите планы). 
-- Напишите ваши рассуждения по поводу оптимизации. 

-- 5. Объясните, что делает и оптимизируйте запрос

--SET STATISTICS IO, TIME ON
SELECT 
	Invoices.InvoiceID, 
	Invoices.InvoiceDate,
	(SELECT People.FullName
		FROM Application.People
		WHERE People.PersonID = Invoices.SalespersonPersonID
	) AS SalesPersonName,
	SalesTotals.TotalSumm AS TotalSummByInvoice, 
	(SELECT SUM(OrderLines.PickedQuantity*OrderLines.UnitPrice)
		FROM Sales.OrderLines
		WHERE OrderLines.OrderId = (SELECT Orders.OrderId 
			FROM Sales.Orders
			WHERE Orders.PickingCompletedWhen IS NOT NULL	
				AND Orders.OrderId = Invoices.OrderId)	
	) AS TotalSummForPickedItems
FROM Sales.Invoices 
	JOIN
	(SELECT InvoiceId, SUM(Quantity*UnitPrice) AS TotalSumm
	FROM Sales.InvoiceLines
	GROUP BY InvoiceId
	HAVING SUM(Quantity*UnitPrice) > 27000) AS SalesTotals
		ON Invoices.InvoiceID = SalesTotals.InvoiceID
ORDER BY TotalSumm DESC

-- --
