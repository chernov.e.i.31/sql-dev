CREATE TABLE [Movies] (
  [Id] integer PRIMARY KEY,
  [Name] nvarchar(255),
  [Year] int,
  [Budget] decimal,
  [Director] int,
  [Description] nvarchar(255)
)
GO

CREATE TABLE [Persons] (
  [Id] integer PRIMARY KEY,
  [Name] nvarchar(255),
  [DoB] datetime
)
GO

CREATE TABLE [Countries] (
  [Id] integer PRIMARY KEY,
  [Name] nvarchar(255)
)
GO

CREATE TABLE [Genres] (
  [Id] integer PRIMARY KEY,
  [Name] nvarchar(255)
)
GO

CREATE TABLE [Premiere] (
  [MovieId] integer,
  [CountrieId] integer,
  [PremiereDate] date,
  [Tax] decimal,
  [LocalName] nvarchar(255)
)
GO

CREATE TABLE [CountriesOfOrigin] (
  [MovieId] integer,
  [CountrieId] integer
)
GO

CREATE TABLE [Actors] (
  [MovieId] integer,
  [PersonId] integer,
  [Role] nvarchar(255)
)
GO

CREATE TABLE [GenresOfMovies] (
  [MovieId] integer,
  [GenreId] integer
)
GO

ALTER TABLE [Premiere] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO

ALTER TABLE [Premiere] ADD FOREIGN KEY ([CountrieId]) REFERENCES [Countries] ([Id])
GO

ALTER TABLE [CountriesOfOrigin] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO

ALTER TABLE [CountriesOfOrigin] ADD FOREIGN KEY ([CountrieId]) REFERENCES [Countries] ([Id])
GO

ALTER TABLE [Persons] ADD FOREIGN KEY ([Id]) REFERENCES [Movies] ([Director])
GO

ALTER TABLE [Actors] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO

ALTER TABLE [Actors] ADD FOREIGN KEY ([PersonId]) REFERENCES [Persons] ([Id])
GO

ALTER TABLE [GenresOfMovies] ADD FOREIGN KEY ([GenreId]) REFERENCES [Genres] ([Id])
GO

ALTER TABLE [GenresOfMovies] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO
