/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

select * from Sales.Customers
/*
1. Довставлять в базу пять записей используя insert в таблицу Customers или Suppliers
*/

insert Sales.Customers (
	[CustomerID],[CustomerName],[BillToCustomerID],[CustomerCategoryID],[BuyingGroupID],[PrimaryContactPersonID],[AlternateContactPersonID]
	,[DeliveryMethodID],[DeliveryCityID],[PostalCityID],[CreditLimit],[AccountOpenedDate],[StandardDiscountPercentage],[IsStatementSent]
	,[IsOnCreditHold],[PaymentDays],[PhoneNumber],[FaxNumber],[DeliveryRun],[RunPosition],[WebsiteURL]
	,[DeliveryAddressLine1],[DeliveryAddressLine2],[DeliveryPostalCode],[DeliveryLocation],[PostalAddressLine1],[PostalAddressLine2],[PostalPostalCode]
	,[LastEditedBy])
values 
	(1062,	N'Покупатель 1',	1069,	5,	NULL,	3261,	NULL, 3,	19881,	19881,	1000.00,	'2023-01-01',	0.000,	0,	
	0,	7,	'(999) 999-9999',	'(999) 999-9999',	NULL,	NULL,	'http://www.microsoft.com/', 'Shop 99',	N'Улица 1',	90243,	geography::STGeomFromText('LINESTRING(-122.360 47.656, -122.343 47.656 )', 4326),	'PO Box 9999',	N'Город',	90243,	
	1),
	(1063,	N'Покупатель 2',	1063,	5,	NULL,	3261,	NULL, 3,	19881,	19881,	2000.00,	'2023-01-01',	0.000,	0,	
	0,	7,	'(999) 999-9999',	'(999) 999-9999',	NULL,	NULL,	'http://www.microsoft.com/', 'Shop 99',	N'Улица 2',	90243,	geography::STGeomFromText('LINESTRING(-122.360 47.656, -122.343 47.656 )', 4326),	'PO Box 9999',	N'Город',	90243,	
	1),
	(1064,	N'Покупатель 3',	1064,	5,	NULL,	3261,	NULL, 3,	19881,	19881,	3000.00,	'2023-01-01',	0.000,	0,	
	0,	7,	'(999) 999-9999',	'(999) 999-9999',	NULL,	NULL,	'http://www.microsoft.com/', 'Shop 99',	N'Улица 3',	90243,	geography::STGeomFromText('LINESTRING(-122.360 47.656, -122.343 47.656 )', 4326),	'PO Box 9999',	N'Город',	90243,	
	1),
	(1065,	N'Покупатель 4',	1065,	5,	NULL,	3261,	NULL, 3,	19881,	19881,	4000.00,	'2023-01-01',	0.000,	0,	
	0,	7,	'(999) 999-9999',	'(999) 999-9999',	NULL,	NULL,	'http://www.microsoft.com/', 'Shop 99',	N'Улица 4',	90243,	geography::STGeomFromText('LINESTRING(-122.360 47.656, -122.343 47.656 )', 4326),	'PO Box 9999',	N'Город',	90243,	
	1),
	(1066,	N'Покупатель 5',	1066,	5,	NULL,	3261,	NULL, 3,	19881,	19881,	5000.00,	'2023-01-01',	0.000,	0,	
	0,	7,	'(999) 999-9999',	'(999) 999-9999',	NULL,	NULL,	'http://www.microsoft.com/', 'Shop 99',	N'Улица 5',	90243,	geography::STGeomFromText('LINESTRING(-122.360 47.656, -122.343 47.656 )', 4326),	'PO Box 9999',	N'Город',	90243,	
	1)


/*
2. Удалите одну запись из Customers, которая была вами добавлена
*/

delete Sales.Customers
where [CustomerName] = 'Покупатель 3'

/*
3. Изменить одну запись, из добавленных через UPDATE
*/

update Sales.Customers
set [PhoneNumber] = '(999) 999-9998'
where [CustomerName] = 'Покупатель 2'

/*
4. Написать MERGE, который вставит вставит запись в клиенты, если ее там нет, и изменит если она уже есть
*/

----Make Target
SELECT * INTO Sales.Customers2
FROM Sales.Customers;

DELETE FROM Sales.Customers2
WHERE [CustomerID] > 1061;

SELECT *
FROM Sales.Customers2

----------

MERGE Sales.Customers2 AS Target
USING Sales.Customers AS Source
    ON (Target.[CustomerID] = Source.[CustomerID])
WHEN MATCHED 
    THEN UPDATE 
        SET [CustomerName] = Source.[CustomerName] + ' New'
WHEN NOT MATCHED 
    THEN INSERT ([CustomerID],[CustomerName],[BillToCustomerID],[CustomerCategoryID],[BuyingGroupID],[PrimaryContactPersonID],[AlternateContactPersonID]
		,[DeliveryMethodID],[DeliveryCityID],[PostalCityID],[CreditLimit],[AccountOpenedDate],[StandardDiscountPercentage],[IsStatementSent]
		,[IsOnCreditHold],[PaymentDays],[PhoneNumber],[FaxNumber],[DeliveryRun],[RunPosition],[WebsiteURL]
		,[DeliveryAddressLine1],[DeliveryAddressLine2],[DeliveryPostalCode],[DeliveryLocation],[PostalAddressLine1],[PostalAddressLine2],[PostalPostalCode]
		,[LastEditedBy], ValidFrom, ValidTo)
    VALUES (Source.[CustomerID],Source.[CustomerName],Source.[BillToCustomerID],Source.[CustomerCategoryID],Source.[BuyingGroupID],Source.[PrimaryContactPersonID],Source.[AlternateContactPersonID]
	,Source.[DeliveryMethodID],Source.[DeliveryCityID],Source.[PostalCityID],Source.[CreditLimit],Source.[AccountOpenedDate],Source.[StandardDiscountPercentage],Source.[IsStatementSent]
	,Source.[IsOnCreditHold],Source.[PaymentDays],Source.[PhoneNumber],Source.[FaxNumber],Source.[DeliveryRun],Source.[RunPosition],Source.[WebsiteURL]
	,Source.[DeliveryAddressLine1],Source.[DeliveryAddressLine2],Source.[DeliveryPostalCode],Source.[DeliveryLocation],Source.[PostalAddressLine1],Source.[PostalAddressLine2],Source.[PostalPostalCode]
	,Source.[LastEditedBy], Source.ValidFrom, Source.ValidTo)
OUTPUT $action, inserted.*;

/*
5. Напишите запрос, который выгрузит данные через bcp out и загрузить через bulk insert
*/

--bcp WideWorldImporters.Sales.Customers out D:\WideWorldImporters.Sales.Customers.txt -c -U SA -P Password123

-- создадим таблицу
CREATE TABLE [Sales].[Customers_Copy](
	[CustomerID] [int] NOT NULL,
	[CustomerName] [nvarchar](100) NOT NULL,
	[BillToCustomerID] [int] NOT NULL,
	[CustomerCategoryID] [int] NOT NULL,
	[BuyingGroupID] [int] NULL,
	[PrimaryContactPersonID] [int] NOT NULL,
	[AlternateContactPersonID] [int] NULL,
	[DeliveryMethodID] [int] NOT NULL,
	[DeliveryCityID] [int] NOT NULL,
	[PostalCityID] [int] NOT NULL,
	[CreditLimit] [decimal](18, 2) NULL,
	[AccountOpenedDate] [date] NOT NULL,
	[StandardDiscountPercentage] [decimal](18, 3) NOT NULL,
	[IsStatementSent] [bit] NOT NULL,
	[IsOnCreditHold] [bit] NOT NULL,
	[PaymentDays] [int] NOT NULL,
	[PhoneNumber] [nvarchar](20) NOT NULL,
	[FaxNumber] [nvarchar](20) NOT NULL,
	[DeliveryRun] [nvarchar](5) NULL,
	[RunPosition] [nvarchar](5) NULL,
	[WebsiteURL] [nvarchar](256) NOT NULL,
	[DeliveryAddressLine1] [nvarchar](60) NOT NULL,
	[DeliveryAddressLine2] [nvarchar](60) NULL,
	[DeliveryPostalCode] [nvarchar](10) NOT NULL,
	[DeliveryLocation] [geography] NULL,
	[PostalAddressLine1] [nvarchar](60) NOT NULL,
	[PostalAddressLine2] [nvarchar](60) NULL,
	[PostalPostalCode] [nvarchar](10) NOT NULL,
	[LastEditedBy] [int] NOT NULL,
	[ValidFrom] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL,
	[ValidTo] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL,
 CONSTRAINT [PK_Sales_Customers_Copy] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [USERDATA],
 CONSTRAINT [UQ_Sales_Customers_Copy_CustomerName] UNIQUE NONCLUSTERED 
(
	[CustomerName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [USERDATA],
	PERIOD FOR SYSTEM_TIME ([ValidFrom], [ValidTo])
) ON [USERDATA] TEXTIMAGE_ON [USERDATA]


BULK INSERT Sales.Customers_Copy
    FROM "D:\WideWorldImporters.Sales.Customers.txt"
	WITH 
		(
		BATCHSIZE = 1000,
		DATAFILETYPE = 'char',
		FIELDTERMINATOR = '\t',
		ROWTERMINATOR ='\n',
		KEEPNULLS,
		TABLOCK
		);