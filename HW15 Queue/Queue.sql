use Movies

--Включить брокер
USE master
ALTER DATABASE Movies
SET ENABLE_BROKER  WITH ROLLBACK IMMEDIATE;


--Создаем типы сообщений
USE Movies
-- For Request
CREATE MESSAGE TYPE [//Movies/SB/RequestMessage] 
	VALIDATION=WELL_FORMED_XML; --служит исключительно для проверки, что данные соответствуют типу XML(но можно любой тип)
-- For Reply
CREATE MESSAGE TYPE [//Movies/SB/ReplyMessage]
	VALIDATION=WELL_FORMED_XML; --служит исключительно для проверки, что данные соответствуют типу XML(но можно любой тип) 

--Создаем контракт(определяем какие сообщения в рамках этого контракта допустимы)
CREATE CONTRACT [//Movies/SB/Contract] (
	[//Movies/SB/RequestMessage] SENT BY INITIATOR,
    [//Movies/SB/ReplyMessage] SENT BY TARGET );

--Создаем ОЧЕРЕДЬ таргета(настрим позже т.к. через ALTER можно ею рулить еще
CREATE QUEUE TargetQueueMovies;
--и сервис таргета
CREATE SERVICE [//Movies/SB/TargetService]
	ON QUEUE TargetQueueMovies ([//Movies/SB/Contract]);

--то же для ИНИЦИАТОРА
CREATE QUEUE InitiatorQueueMovies;

CREATE SERVICE [//Movies/SB/InitiatorService]
	ON QUEUE InitiatorQueueMovies ([//Movies/SB/Contract]);

--CreateMessage.sql
--GetMessage.sql
--ConfirmMessage.sql


ALTER QUEUE [dbo].[InitiatorQueueMovies] WITH 
	STATUS = ON --OFF=очередь НЕ доступна(ставим если глобальные проблемы)
    ,RETENTION = OFF --ON=все завершенные сообщения хранятся в очереди до окончания диалога
	,POISON_MESSAGE_HANDLING (STATUS = OFF) --ON=после 5 ошибок очередь будет отключена
	,ACTIVATION (
		STATUS = ON --OFF=очередь не активирует ХП(в PROCEDURE_NAME)(ставим на время исправления ХП, но с потерей сообщений)  
		,PROCEDURE_NAME = dbo.ConfirmMessage
		,MAX_QUEUE_READERS = 1 --количество потоков(ХП одновременно вызванных) при обработке сообщений(0-32767)
							   --(0=тоже не позовется процедура)(ставим на время исправления ХП, без потери сообщений) 
		,EXECUTE AS OWNER --учетка от имени которой запустится ХП
	) 

GO
ALTER QUEUE [dbo].[TargetQueueMovies] 
	WITH STATUS = ON 
    ,RETENTION = OFF 
	,POISON_MESSAGE_HANDLING (STATUS = OFF)
	,ACTIVATION (
		STATUS = ON 
		,PROCEDURE_NAME = dbo.GetActorToMovie
		,MAX_QUEUE_READERS = 1
		,EXECUTE AS OWNER 
	) 

GO

exec [dbo].[ConnectActorToMovie] @ActorId =1,@MovieId = 1,@RoleId = 1,@RoleName = 'qwww'



--очистка
--DROP SERVICE [//Movies/SB/TargetService]
--DROP SERVICE [//Movies/SB/InitiatorService]
--DROP QUEUE [dbo].[TargetQueueMovies]
--DROP QUEUE [dbo].[InitiatorQueueMovies]
--DROP CONTRACT [//Movies/SB/Contract]
--DROP MESSAGE TYPE [//Movies/SB/RequestMessage]
--DROP MESSAGE TYPE [//Movies/SB/ReplyMessage]
--DROP PROCEDURE IF EXISTS  dbo.ConnectActorToMovie;
--DROP PROCEDURE IF EXISTS  Sales.GetNewInvoice;
--DROP PROCEDURE IF EXISTS  Sales.ConfirmInvoice;

