CREATE PROCEDURE dbo.GetActorToMovie --будет получать сообщение на таргете
AS
BEGIN

	DECLARE @TargetDlgHandle UNIQUEIDENTIFIER,
			@Message NVARCHAR(4000),
			@MessageType Sysname,
			@ReplyMessage NVARCHAR(4000),
			@ReplyMessageName Sysname,
			@ActorId INT,
			@MovieId INT,
			@RoleId INT,
			@RoleName Nvarchar(100),
			@xml XML; 
	
	BEGIN TRAN; 

	--Получаем сообщение от инициатора которое находится у таргета
	RECEIVE TOP(1) --обычно одно сообщение, но можно пачкой
		@TargetDlgHandle = Conversation_Handle, --ИД диалога
		@Message = Message_Body, --само сообщение
		@MessageType = Message_Type_Name --тип сообщения( в зависимости от типа можно по разному обрабатывать) обычно два - запрос и ответ
	FROM dbo.TargetQueueMovies; --имя очереди которую мы ранее создавали

	SET @xml = CAST(@Message AS XML);

	--достали ИД
	SELECT @ActorId = R.tbl.value('@ActorId','INT'),
		@MovieId = R.tbl.value('@MovieId','INT'),
		@RoleId  = R.tbl.value('@RoleId','INT'),
		@RoleName = R.tbl.value('@RoleName','Nvarchar(100)')
	FROM @xml.nodes('/RequestMessage/tbl') as R(tbl);
	
	IF EXISTS (SELECT * FROM dbo.Persons WHERE id = @ActorId) 
		and EXISTS(SELECT * FROM dbo.Movies WHERE id = @MovieId)
		and EXISTS(SELECT * FROM dbo.Roles WHERE id = @RoleId)
	BEGIN
		insert dbo.Actors (MovieId, PersonId, RoleId, RoleName) 
		values (@MovieId, @ActorId, @RoleId, @RoleName )
	END;
	
	-- Confirm and Send a reply
	IF @MessageType=N'//Movies/SB/RequestMessage' --если наш тип сообщения
	BEGIN
		SET @ReplyMessage =N'<ReplyMessage> Message received</ReplyMessage>'; --ответ
	    --отправляем сообщение нами придуманное, что все прошло хорошо
		SEND ON CONVERSATION @TargetDlgHandle
			MESSAGE TYPE [//Movies/SB/ReplyMessage](@ReplyMessage);
		END CONVERSATION @TargetDlgHandle; --А вот и завершение диалога!!! - оно двухстороннее(пока-пока) ЭТО первый ПОКА
		                                   --НЕЛЬЗЯ ЗАВЕРШАТЬ ДИАЛОГ ДО ОТПРАВКИ ПЕРВОГО СООБЩЕНИЯ
	END 
	
	COMMIT TRAN;
END