CREATE PROCEDURE dbo.ConnectActorToMovie
	@ActorId INT,
	@MovieId INT,
	@RoleId INT,
	@RoleName Nvarchar(100)

AS
BEGIN
	SET NOCOUNT ON;

    --Sending a Request Message to the Target	
	DECLARE @InitDlgHandle UNIQUEIDENTIFIER;
	DECLARE @RequestMessage NVARCHAR(4000);


	
	BEGIN TRAN --на всякий случай в транзакции, т.к. это еще не относится к транзакции ПЕРЕДАЧИ сообщения

	--Формируем XML с корнем RequestMessage где передадим номер инвойса(в принцыпе сообщение может быть любым)
	SELECT @RequestMessage = (
		SELECT * 
		from (			values (@ActorId, @MovieId, @RoleId, @RoleName)
			) as tbl(ActorId, MovieId, RoleId, RoleName)); 
	
	
	--Создаем диалог
	BEGIN DIALOG @InitDlgHandle
		FROM SERVICE [//Movies/SB/InitiatorService] --от этого сервиса(это сервис текущей БД, поэтому он НЕ строка)
		TO SERVICE '//Movies/SB/TargetService'		--к этому сервису(это сервис который может быть где-то, поэтому строка)
		ON CONTRACT [//Movies/SB/Contract]			--в рамках этого контракта
		WITH ENCRYPTION=OFF;						--не шифрованный

	--отправляем одно наше подготовленное сообщение, но можно отправить и много сообщений, которые будут обрабатываться строго последовательно)
	SEND ON CONVERSATION @InitDlgHandle 
	MESSAGE TYPE [//Movies/SB/RequestMessage] (@RequestMessage);
	
	COMMIT TRAN 
END
GO