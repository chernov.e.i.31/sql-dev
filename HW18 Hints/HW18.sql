SET STATISTICS IO, TIME ON;

DROP TABLE IF EXISTS #cust

SELECT ordTotal.CustomerID
into  #cust
FROM Sales.OrderLines AS Total
	Join Sales.Orders AS ordTotal On ordTotal.OrderID = Total.OrderID
group by ordTotal.CustomerID
having  SUM(Total.UnitPrice*Total.Quantity) >250000


Select ord.CustomerID, 
	det.StockItemID, 
	SUM(det.UnitPrice), 
	SUM(det.Quantity), 
	COUNT(ord.OrderID)
FROM Sales.Orders AS ord
	JOIN Sales.OrderLines AS det ON det.OrderID = ord.OrderID
	JOIN Sales.Invoices AS Inv ON Inv.OrderID = ord.OrderID
	JOIN Warehouse.StockItemTransactions AS ItemTrans ON ItemTrans.StockItemID = det.StockItemID
	JOIN Warehouse.StockItems AS It ON It.StockItemID = ItemTrans.StockItemID
	JOIN #cust AS Cust ON Cust.CustomerID = Inv.CustomerID
WHERE Inv.BillToCustomerID != ord.CustomerID
	and IT.SupplierID = 12
	AND Inv.InvoiceDate = ord.OrderDate
GROUP BY Cust.CustomerID, det.StockItemID
ORDER BY Cust.CustomerID, det.StockItemID