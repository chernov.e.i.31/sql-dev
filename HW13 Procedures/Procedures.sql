/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "18 - Хранимые процедуры, функции, триггеры, курсоры".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

USE WideWorldImporters

/*
Во всех заданиях написать хранимую процедуру / функцию и продемонстрировать ее использование.
*/

/*
1) Написать функцию возвращающую Клиента с наибольшей суммой покупки.
*/

IF OBJECT_ID (N'dbo.fClientWithMaxOrders', N'IF') IS NOT NULL
    DROP FUNCTION dbo.fClientWithMaxOrders;
GO
CREATE FUNCTION dbo.fClientWithMaxOrders()
RETURNS TABLE
AS
RETURN
(
    select top 1 c.CustomerID, c.CustomerName, sum(il.ExtendedPrice) orderSum
	from Sales.Customers c
		join Sales.Invoices i on c.CustomerID = i.CustomerID
		join Sales.InvoiceLines il on il.InvoiceLineID = i.InvoiceID 
	group by c.CustomerID, c.CustomerName
	order by sum(il.ExtendedPrice) desc
);

select * from dbo.fClientWithMaxOrders()

/*
2) Написать хранимую процедуру с входящим параметром СustomerID, выводящую сумму покупки по этому клиенту.
Использовать таблицы :
Sales.Customers
Sales.Invoices
Sales.InvoiceLines
*/

IF OBJECT_ID(N'dbo.pClientWithMaxOrders', N'P') IS NOT NULL
DROP PROCEDURE dbo.pClientWithMaxOrders;
GO
CREATE PROCEDURE dbo.pClientWithMaxOrders(@ClientId int)
WITH EXECUTE AS CALLER
AS
    SET NOCOUNT ON;

    select sum(il.ExtendedPrice) orderSum
	from Sales.Customers c
		join Sales.Invoices i on c.CustomerID = i.CustomerID
		join Sales.InvoiceLines il on il.InvoiceLineID = i.InvoiceID 
	where i.CustomerID = @ClientId
	
EXEC dbo.pClientWithMaxOrders @ClientId = 14

/*
3) Создать одинаковую функцию и хранимую процедуру, посмотреть в чем разница в производительности и почему.
*/
IF OBJECT_ID (N'dbo.fClients', N'IF') IS NOT NULL
    DROP FUNCTION dbo.fClients;
GO
CREATE FUNCTION dbo.fClients(@ClientN varchar)
RETURNS TABLE
AS
RETURN
(
	select CustomerName
	from Sales.Customers
	where CustomerName like  @ClientN + '%'
)

IF OBJECT_ID (N'dbo.pClients', N'P') IS NOT NULL
    DROP PROCEDURE dbo.pClients
GO
CREATE PROCEDURE dbo.pClients(@ClientN varchar)
WITH EXECUTE AS CALLER
AS
	select CustomerName
	from Sales.Customers
	where CustomerName like @ClientN + '%'

select * from dbo.fClients('A')
EXEC dbo.pClients 'A'

/*
разницы в производительности нет. т.к. запрос выполняет одни и те же действия. 
у функции же есть преимущество в плане встраивания в уже имеющиеся запросы
*/

/*
4) Создайте табличную функцию покажите как ее можно вызвать для каждой строки result set'а без использования цикла. 
*/

IF OBJECT_ID (N'dbo.fOrdersSumByClient', N'IF') IS NOT NULL
    DROP FUNCTION dbo.fOrdersSumByClient;
GO
CREATE FUNCTION dbo.fOrdersSumByClient(@ClientId int)
RETURNS TABLE
AS
RETURN
(
    select sum(il.ExtendedPrice) orderSum
	from Sales.Invoices i 
		join Sales.InvoiceLines il on il.InvoiceLineID = i.InvoiceID 
	where @ClientId = i.CustomerID
	
);

select c.CustomerID, c.CustomerName, f.orderSum 
from Sales.Customers c
	outer apply dbo.fOrdersSumByClient(c.CustomerID) f

