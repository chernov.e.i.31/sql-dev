/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "06 - Оконные функции".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters
/*
1. Сделать расчет суммы продаж нарастающим итогом по месяцам с 2015 года 
(в рамках одного месяца он будет одинаковый, нарастать будет в течение времени выборки).
Выведите: id продажи, название клиента, дату продажи, сумму продажи, сумму нарастающим итогом

Пример:
-------------+----------------------------
Дата продажи | Нарастающий итог по месяцу
-------------+----------------------------
 2015-01-29   | 4801725.31
 2015-01-30	 | 4801725.31
 2015-01-31	 | 4801725.31
 2015-02-01	 | 9626342.98
 2015-02-02	 | 9626342.98
 2015-02-03	 | 9626342.98
Продажи можно взять из таблицы Invoices.
Нарастающий итог должен быть без оконной функции.
*/

select i.InvoiceID,
	c.CustomerName ,
	i.InvoiceDate,
	(select SUM(li.ExtendedPrice) from Sales.InvoiceLines li where li.InvoiceID = i.InvoiceID) sumByInvoice,
	(select  SUM(li2.ExtendedPrice) 
		from Sales.Invoices i2
			join Sales.InvoiceLines li2 on li2.InvoiceID = i2.InvoiceID
		where i2.InvoiceDate >= '20150101' 
			and DATEADD(month, DATEDIFF(month, 0, i.InvoiceDate), 0) >= DATEADD(month, DATEDIFF(month, 0, i2.InvoiceDate), 0)
		) nitog,
	(select  SUM(li2.ExtendedPrice) 
		from Sales.Invoices i2
			join Sales.InvoiceLines li2 on li2.InvoiceID = i2.InvoiceID
		where DATEADD(month, DATEDIFF(month, 0, i.InvoiceDate), 0) = DATEADD(month, DATEDIFF(month, 0, i2.InvoiceDate), 0)
		) byMonth
from Sales.Invoices i
	join Sales.Customers c on c.CustomerID = i.CustomerID
where i.InvoiceDate >= '20150101'  
order by i.InvoiceDate, i.InvoiceID


/*
2. Сделайте расчет суммы нарастающим итогом в предыдущем запросе с помощью оконной функции.
   Сравните производительность запросов 1 и 2 с помощью set statistics time, io on
*/

;with sums as (
	select i.CustomerID,
	i.InvoiceID,
	i.InvoiceDate,
	DATEADD(month, DATEDIFF(month, 0, i.InvoiceDate), 0) InvoiceMonth,
	SUM(li.ExtendedPrice) sumByInvoice
	from Sales.Invoices i
		join Sales.InvoiceLines li on li.InvoiceID = i.InvoiceID
	where i.InvoiceDate >= '20150101'
	group by i.CustomerID, i.InvoiceID, i.InvoiceDate
)

select s.InvoiceID,
	c.CustomerName ,
	s.InvoiceDate, 
	s.sumByInvoice,
	sum(s.sumByInvoice) over (order by s.InvoiceMonth) nitog,
	sum(s.sumByInvoice) over (partition by DATEADD(month, DATEDIFF(month, 0, s.InvoiceDate), 0)) byMonth
from sums s
	join Sales.Customers c on c.CustomerID = s.CustomerID

order by s.InvoiceDate, s.InvoiceID


/*
3. Вывести список 2х самых популярных продуктов (по количеству проданных) 
в каждом месяце за 2016 год (по 2 самых популярных продукта в каждом месяце).
*/

;with items as (
	select DATEADD(month, DATEDIFF(month, 0, i.InvoiceDate), 0) monthOfInvoice, 
		li.Description,
		sum(li.Quantity) countOfItems,
		row_number() over (partition by DATEADD(month, DATEDIFF(month, 0, i.InvoiceDate), 0) order by sum(li.Quantity) desc	 ) ordNum
	from Sales.Invoices i
		join Sales.InvoiceLines li on li.InvoiceID = i.InvoiceID
	where i.InvoiceDate between '20160101' and '20161231'
	group by DATEADD(month, DATEDIFF(month, 0, i.InvoiceDate), 0), li.Description
)

select * 
from items i
where i.ordNum<3
order by i.monthOfInvoice, i.ordNum 


/*
4. Функции одним запросом
Посчитайте по таблице товаров (в вывод также должен попасть ид товара, название, брэнд и цена):
* пронумеруйте записи по названию товара, так чтобы при изменении буквы алфавита нумерация начиналась заново
* посчитайте общее количество товаров и выведете полем в этом же запросе
* посчитайте общее количество товаров в зависимости от первой буквы названия товара
* отобразите следующий id товара исходя из того, что порядок отображения товаров по имени 
* предыдущий ид товара с тем же порядком отображения (по имени)
* названия товара 2 строки назад, в случае если предыдущей строки нет нужно вывести "No items"
* сформируйте 30 групп товаров по полю вес товара на 1 шт

Для этой задачи НЕ нужно писать аналог без аналитических функций.
*/

select StockItemID,
	StockItemName,
	Brand,
	UnitPrice,
	row_number() over (partition by substring(StockItemName, 1,1 )  order by StockItemName  ) strNumByFirstLetter,
	count(StockItemID) over () countItems,
	count(StockItemID) over (partition by substring(StockItemName, 1,1 )  order by StockItemName  ) countItemsByFirstLetter,
	lead(StockItemID,1) over (order by StockItemName ) nextIdByName,
	lag(StockItemID,1) over (order by StockItemName ) prevIdByName,
	lag(StockItemName,2, 'No items') over (order by StockItemID ) prevNameById,
	ntile(30) over (order by TypicalWeightPerUnit) groupByWeith
from Warehouse.StockItems
order by StockItemName

/*
5. По каждому сотруднику выведите последнего клиента, которому сотрудник что-то продал.
   В результатах должны быть ид и фамилия сотрудника, ид и название клиента, дата продажи, сумму сделки.
*/
;with invoices as (
	select i.CustomerId,
		p.FullName,
		p.PersonID,
		i.InvoiceDate,
		sum(il.ExtendedPrice) orderPrice ,
		row_number() over (partition by i.CustomerId order by i.InvoiceDate desc ) strNum
	from Sales.Invoices i
		join Sales.InvoiceLines il on i.InvoiceID = il.InvoiceID
		join Application.People p on p.PersonID = i.AccountsPersonID
	group by p.FullName,
		p.PersonID,
		i.InvoiceDate,
		i.CustomerId
)

select  c.CustomerName , ii.CustomerId, ii.FullName, ii.PersonID, ii.InvoiceDate, ii.orderPrice
from Sales.Customers c 
	join invoices ii on ii.CustomerID = c.CustomerID
where ii.strNum  = 1
/*
6. Выберите по каждому клиенту два самых дорогих товара, которые он покупал.
В результатах должно быть ид клиета, его название, ид товара, цена, дата покупки.
*/
;with dataInv as (
select p.PersonID,
		p.FullName,
		il.StockItemID,
		il.UnitPrice,
		max (i.InvoiceDate) orderDate,
		row_number() over (partition by p.PersonID order by il.UnitPrice desc ) strNum
	from Sales.Invoices i
		join Sales.InvoiceLines il on i.InvoiceID = il.InvoiceID
		join Application.People p on p.PersonID = i.AccountsPersonID
	group by p.PersonID,
		p.FullName,
		il.StockItemID,
		il.UnitPrice
)
select * from dataInv
where strNum<3