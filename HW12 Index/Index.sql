use [Movies];

--поиск по странам
create unique index idx_country_name on [Countries]([Name]);
--связь страны и фильма
create unique index idx_countryoforigin on [CountriesOfOrigin](MovieId, CountrieId);
--список фильмов по дате выхода в прокат
create unique index idx_premiere on [Premiere](PremiereDate) include (MovieId, CountrieId);
--поиск по имени фильма
create index idx_premiere_name on [Premiere](LocalName) include (MovieId);
--поиск по имени фильма
create index idx_movie_name on [Movies]([Name]);
--поиск по году выхода фильма
create index idx_movie_year on [Movies]([Year]);
--поиск по жанру
create unique index idx_genre_name on [Genres]([Name]);
--поиск по типу роли
create unique index idx_role_name on [Roles]([Name]);
--поиск по имени актера
create index idx_person_name on [Persons]([Name]);
--связка фильма актера и роли
create unique index idx_actors on [Actors](MovieId,PersonId,RoleId) include (RoleName);

--связка фильма жанра и фильма
create unique index idx_genresofmovies on [GenresOfMovies](GenreId, MovieId); 

-- поиск фильмов произведенных в определенной стране начиная с указанного года
select m.Name, m.Year, m.Description, p.LocalName
from Countries c
	join CountriesOfOrigin co on co.CountrieId = c.Id
	join Movies m on m.Id = co.MovieId
	left join Premiere p on p.MovieId = m.Id and p.CountrieId = c.Id
where c.Name like 'Россия'
	and m.Year > 2020

-- поиск фильмов в которых снимался актер
select m.Name, m.Year, m.Description, r.Name, a.RoleName
from Persons p
	join Actors a on a.PersonId = p.Id
	join Movies m on a.MovieId = m.Id
	join Roles r on a.RoleId = r.Id
where p.Name ='Name'

-- поиск фильмов выходящих в определенной стране в указанном периоде
select m.Name, m.Year, m.Description, p.LocalName
from Countries c
	join Premiere p on p.CountrieId = c.Id
	join Movies m on m.Id = p.MovieId
where c.Name like 'Россия'
	and p.PremiereDate between '20220101' and '20221001'

-- поиск фильмов по названию
select m.Name, m.Year, m.Description, p.LocalName
from Movies m 
	left join Premiere p on p.MovieId = m.Id 
where m.Name like 'Name%' or p.LocalName like 'Name%'

-- поиск фильмов по жанру
select distinct m.Name
from Genres g
	join GenresOfMovies gm on gm.GenreId = g.Id
	join Movies m on m.Id = gm.MovieId
where g.Name in ('q1', 'q2')