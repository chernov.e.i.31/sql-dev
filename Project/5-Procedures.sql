

create or alter procedure dbo.AddCountrie @cName NVARCHAR(100),  @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		IF not EXISTS (SELECT * FROM dbo.Countries WHERE Name = @cName)
		begin
			insert dbo.Countries (Name) 
			values (@cName)
			set @result = 'Успех'
		end
		else 
		begin
			set @result = 'Запись уже существует'
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddCountriesOfOrigin @Movie int, @Countrie int,  @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		IF not EXISTS (SELECT * FROM dbo.Countries WHERE id = @Countrie)
		begin
			set @result = 'Страна не существует'
		end
		IF not EXISTS (SELECT * FROM dbo.Movies WHERE id = @Movie)
		begin
			set @result = 'Фильм не существует'
		end
		if @result is null
		begin
			IF not EXISTS (SELECT * FROM dbo.CountriesOfOrigin WHERE MovieId = @Movie and CountrieId = @Countrie)
			begin
				insert dbo.CountriesOfOrigin (MovieId ,CountrieId) 
				values (@Movie, @Countrie)
				set @result = 'Успех'
			end
			else 
			begin
				set @result = 'Запись уже существует'
			end
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddGenres @cName NVARCHAR(100), @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		IF not EXISTS (SELECT * FROM dbo.Genres WHERE Name = @cName)
		begin
			insert dbo.Genres (Name) 
			values (@cName)
			set @result = 'Успех'
		end
		else 
		begin
			set @result = 'Запись уже существует'
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddGenresOfMovies @Movie int, @Genre int,  @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		IF not EXISTS (SELECT * FROM dbo.Genres WHERE id = @Genre)
		begin
			set @result = 'Жанр не существует'
		end
		IF not EXISTS (SELECT * FROM dbo.Movies WHERE id = @Movie)
		begin
			set @result = 'Фильм не существует'
		end
		if @result is null
		begin
			IF not EXISTS (SELECT * FROM dbo.GenresOfMovies WHERE MovieId = @Movie and GenreId = @Genre)
			begin
				insert dbo.GenresOfMovies (MovieId ,GenreId) 
				values (@Movie, @Genre)
				set @result = 'Успех'
			end
			else 
			begin
				set @result = 'Запись уже существует'
			end
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddMovie @cName NVARCHAR(100), @year int, @budget decimal(18,0), @description NVARCHAR(700), @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		if @budget <0
		begin
			set @result = 'Бюджет не может быть меньше 0'
		end
		if @result is null
		begin
			insert dbo.Movies(Name,Year, Budget,Description) 
			values (@cName,@year,@budget,@description)
			set @result = 'Успех'
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddPerson @cName NVARCHAR(100), @DoB datetime, @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		begin
			insert dbo.Persons(Name,DoB) 
			values (@cName,@DoB)
			set @result = 'Успех'
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddPremiere @Movie int, @Countrie int, @Date date, @Tax decimal(18,0), @Name NVARCHAR(100), @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		IF not EXISTS (SELECT * FROM dbo.Countries WHERE id = @Countrie)
		begin
			set @result = 'Страны не существует'
		end
		IF not EXISTS (SELECT * FROM dbo.Movies WHERE id = @Movie)
		begin
			set @result = 'Фильм не существует'
		end
		if @Tax <0
		begin
			set @result = 'Бюджет не может быть меньше 0'
		end
		if @result is null
		begin
			IF not EXISTS (SELECT * FROM dbo.Premiere WHERE MovieId = @Movie and CountrieId = @Countrie)
			begin
				insert dbo.Premiere(MovieId ,CountrieId,PremiereDate,Tax,LocalName) 
				values (@Movie, @Countrie,@Date,@Tax,@Name)
				set @result = 'Успех'
			end
			else 
			begin
				set @result = 'Запись уже существует'
			end
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddRoles @cName NVARCHAR(100), @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		IF not EXISTS (SELECT * FROM dbo.Roles WHERE Name = @cName)
		begin
			insert dbo.Roles(Name) 
			values (@cName)
			set @result = 'Успех'
		end
		else 
		begin
			set @result = 'Запись уже существует'
		end
	select @result 
	COMMIT TRAN;
end;

create or alter procedure dbo.AddActors @Movie int,@person int,@Role int,@cName NVARCHAR(100), @result NVARCHAR(100) out as
begin
	BEGIN TRAN; 
		IF not EXISTS (SELECT * FROM dbo.Movies WHERE id = @Movie)
		begin
			set @result = 'Фильма не существует'
		end
		IF not EXISTS (SELECT * FROM dbo.Persons WHERE id = @person)
		begin
			set @result = 'Человека не существует'
		end
		IF not EXISTS (SELECT * FROM dbo.Roles WHERE id = @Role)
		begin
			set @result = 'Роли не существует'
		end
		IF not EXISTS (SELECT * FROM dbo.Actors WHERE RoleName = @cName 
			and MovieId = @Movie
			and PersonId = @person
			and RoleId = @Role
			)
		begin
			insert dbo.Actors(MovieId,PersonId,RoleId,RoleName) 
			values (@Movie,@person,@Role,@cName)
			set @result = 'Успех'
		end
		else 
		begin
			set @result = 'Запись уже существует'
		end
	select @result 
	COMMIT TRAN;
end;