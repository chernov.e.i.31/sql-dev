CREATE DATABASE [Movies]
ON  PRIMARY 
( NAME = movies, FILENAME = N'/var/opt/mssql/data/Movies.mdf' , 
	SIZE = 8MB , 
	MAXSIZE = 5GB, 
	FILEGROWTH = 65536KB ),
FILEGROUP [MovieYear]
( NAME = N'MovieYear', 
  FILENAME = N'/var/opt/mssql/data/MovieYear.ndf' , 
  SIZE = 8MB , 
	MAXSIZE = 5GB,
  FILEGROWTH = 65536KB ) 
 LOG ON 
( NAME = movies_log, FILENAME = N'/var/opt/mssql/data/Movies_log.ldf' , 
	SIZE = 8MB , 
	MAXSIZE = 1GB , 
	FILEGROWTH = 65536KB )
GO