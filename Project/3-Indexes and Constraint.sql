use [Movies];

-- ограничения

ALTER TABLE [Movies] ADD CONSTRAINT YearCheck CHECK ([Year] >=1895);
ALTER TABLE [Persons] ADD CONSTRAINT DoBCheck CHECK ([DoB] >= '18000101');
ALTER TABLE [Persons] ADD CONSTRAINT PersonNameCheck CHECK ([Name] != '');
ALTER TABLE [Countries] ADD CONSTRAINT CountrieNameCheck CHECK ([Name] != '');
ALTER TABLE [Genres] ADD CONSTRAINT GenreNameCheck CHECK ([Name] != '');
ALTER TABLE [Roles] ADD CONSTRAINT RolesNameCheck CHECK ([Name] != '');
ALTER TABLE [Premiere] ADD CONSTRAINT TaxCheck CHECK ([Tax] > 0);
ALTER TABLE [Actors] ADD CONSTRAINT RoleNameCheck CHECK ([RoleName] != '');

-- индексы
create index idx_year on [Movies]([Year]);
create index idx_name on [Countries]([Name]);
--поиск по странам
create unique index idx_country_name on [Countries]([Name]);
--связь страны и фильма
create unique index idx_countryoforigin on [CountriesOfOrigin](MovieId, CountrieId);
--список фильмов по дате выхода в прокат
create unique index idx_premiere on [Premiere](PremiereDate) include (MovieId, CountrieId);
--поиск по имени фильма
create index idx_premiere_name on [Premiere](LocalName) include (MovieId);
--поиск по имени фильма
create index idx_movie_name on [Movies]([Name]);
--поиск по году выхода фильма
--create index idx_movie_year on [Movies]([Year]);
CREATE CLUSTERED INDEX [ClusteredIndex_on_schmMovieYearPartition_638468228183392108] ON [dbo].[Movies]([Year])
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [schmMovieYearPartition]([Year])
--поиск по жанру
create unique index idx_genre_name on [Genres]([Name]);
--поиск по типу роли
create unique index idx_role_name on [Roles]([Name]);
--поиск по имени актера
create index idx_person_name on [Persons]([Name]);
--связка фильма актера и роли
create unique index idx_actors on [Actors](MovieId,PersonId,RoleId) include (RoleName);
--связка фильма жанра и фильма
create unique index idx_genresofmovies on [GenresOfMovies](GenreId, MovieId); 