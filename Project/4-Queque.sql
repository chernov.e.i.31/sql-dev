use [Movies];

--Включить брокер
USE master
ALTER DATABASE Movies
SET ENABLE_BROKER  WITH ROLLBACK IMMEDIATE;


--Создаем типы сообщений
USE Movies
-- For Request
CREATE MESSAGE TYPE [//Movies/SB/RequestMessage] 
	VALIDATION=WELL_FORMED_XML; --служит исключительно для проверки, что данные соответствуют типу XML(но можно любой тип)
-- For Reply
CREATE MESSAGE TYPE [//Movies/SB/ReplyMessage]
	VALIDATION=WELL_FORMED_XML; --служит исключительно для проверки, что данные соответствуют типу XML(но можно любой тип) 

--Создаем контракт(определяем какие сообщения в рамках этого контракта допустимы)
CREATE CONTRACT [//Movies/SB/Contract] (
	[//Movies/SB/RequestMessage] SENT BY INITIATOR,
    [//Movies/SB/ReplyMessage] SENT BY TARGET );

--Создаем ОЧЕРЕДЬ таргета(настрим позже т.к. через ALTER можно ею рулить еще
CREATE QUEUE TargetQueueMovies;
--и сервис таргета
CREATE SERVICE [//Movies/SB/TargetService]
	ON QUEUE TargetQueueMovies ([//Movies/SB/Contract]);

--то же для ИНИЦИАТОРА
CREATE QUEUE InitiatorQueueMovies;

CREATE SERVICE [//Movies/SB/InitiatorService]
	ON QUEUE InitiatorQueueMovies ([//Movies/SB/Contract]);

-- создание сообщения
CREATE PROCEDURE dbo.ConnectActorToMovie
	@ActorId INT,
	@MovieId INT,
	@RoleId INT,
	@RoleName Nvarchar(100)

AS
BEGIN
	SET NOCOUNT ON;

    --Sending a Request Message to the Target	
	DECLARE @InitDlgHandle UNIQUEIDENTIFIER;
	DECLARE @RequestMessage NVARCHAR(4000);

	DROP TABLE IF EXISTS #data;

	create table #data(
		ActorId INT,
		MovieId INT,
		RoleId INT,
		RoleName Nvarchar(100)
	)

	insert #data (ActorId, MovieId, RoleId, RoleName)
	values (@ActorId, @MovieId, @RoleId, @RoleName)
	
	BEGIN TRAN --на всякий случай в транзакции, т.к. это еще не относится к транзакции ПЕРЕДАЧИ сообщения

	--Формируем XML с корнем RequestMessage где передадим номер инвойса(в принцыпе сообщение может быть любым)
	SELECT @RequestMessage = (SELECT *
							  from #data as tbl  
							  FOR XML AUTO, root('RequestMessage')); 	
	
	--Создаем диалог
	BEGIN DIALOG @InitDlgHandle
		FROM SERVICE [//Movies/SB/InitiatorService] --от этого сервиса(это сервис текущей БД, поэтому он НЕ строка)
		TO SERVICE '//Movies/SB/TargetService'		--к этому сервису(это сервис который может быть где-то, поэтому строка)
		ON CONTRACT [//Movies/SB/Contract]			--в рамках этого контракта
		WITH ENCRYPTION=OFF;						--не шифрованный

	--отправляем одно наше подготовленное сообщение, но можно отправить и много сообщений, которые будут обрабатываться строго последовательно)
	SEND ON CONVERSATION @InitDlgHandle 
	MESSAGE TYPE [//Movies/SB/RequestMessage] (@RequestMessage);
	
	COMMIT TRAN 
END
GO

--получение сообщения
CREATE PROCEDURE dbo.GetActorToMovie --будет получать сообщение на таргете
AS
BEGIN

	DECLARE @TargetDlgHandle UNIQUEIDENTIFIER,
			@Message NVARCHAR(4000),
			@MessageType Sysname,
			@ReplyMessage NVARCHAR(4000),
			@ReplyMessageName Sysname,
			@ActorId INT,
			@MovieId INT,
			@RoleId INT,
			@RoleName Nvarchar(100),
			@xml XML; 
	
	BEGIN TRAN; 

	--Получаем сообщение от инициатора которое находится у таргета
	RECEIVE TOP(1) --обычно одно сообщение, но можно пачкой
		@TargetDlgHandle = Conversation_Handle, --ИД диалога
		@Message = Message_Body, --само сообщение
		@MessageType = Message_Type_Name --тип сообщения( в зависимости от типа можно по разному обрабатывать) обычно два - запрос и ответ
	FROM dbo.TargetQueueMovies; --имя очереди которую мы ранее создавали

	SET @xml = CAST(@Message AS XML);

	--достали ИД
	SELECT @ActorId = R.tbl.value('@ActorId','INT'),
		@MovieId = R.tbl.value('@MovieId','INT'),
		@RoleId  = R.tbl.value('@RoleId','INT'),
		@RoleName = R.tbl.value('@RoleName','Nvarchar(100)')
	FROM @xml.nodes('/RequestMessage/tbl') as R(tbl);
	
	IF EXISTS (SELECT * FROM dbo.Persons WHERE id = @ActorId) 
		and EXISTS(SELECT * FROM dbo.Movies WHERE id = @MovieId)
		and EXISTS(SELECT * FROM dbo.Roles WHERE id = @RoleId)
	BEGIN
		insert dbo.Actors (MovieId, PersonId, RoleId, RoleName) 
		values (@MovieId, @ActorId, @RoleId, @RoleName )
	END;
	
	-- Confirm and Send a reply
	IF @MessageType=N'//Movies/SB/RequestMessage' --если наш тип сообщения
	BEGIN
		SET @ReplyMessage =N'<ReplyMessage> Message received</ReplyMessage>'; --ответ
	    --отправляем сообщение нами придуманное, что все прошло хорошо
		SEND ON CONVERSATION @TargetDlgHandle
			MESSAGE TYPE [//Movies/SB/ReplyMessage](@ReplyMessage);
		END CONVERSATION @TargetDlgHandle; --А вот и завершение диалога!!! - оно двухстороннее(пока-пока) ЭТО первый ПОКА
		                                   --НЕЛЬЗЯ ЗАВЕРШАТЬ ДИАЛОГ ДО ОТПРАВКИ ПЕРВОГО СООБЩЕНИЯ
	END 
	
	COMMIT TRAN;
END

--подтверждение сообщения
CREATE PROCEDURE dbo.ConfirmMessage
AS
BEGIN
	--Receiving Reply Message from the Target.	
	DECLARE @InitiatorReplyDlgHandle UNIQUEIDENTIFIER,
			@ReplyReceivedMessage NVARCHAR(1000) 
	
	BEGIN TRAN; 

	    --Получаем сообщение от таргета которое находится у инициатора
		RECEIVE TOP(1)
			@InitiatorReplyDlgHandle=Conversation_Handle
			,@ReplyReceivedMessage=Message_Body
		FROM dbo.InitiatorQueueMovies; 
		
		END CONVERSATION @InitiatorReplyDlgHandle; --ЭТО второй ПОКА
		
		SELECT @ReplyReceivedMessage AS ReceivedRepliedMessage; --не для прода

	COMMIT TRAN; 
END



--включение
ALTER QUEUE [dbo].[InitiatorQueueMovies] WITH 
	STATUS = ON --OFF=очередь НЕ доступна(ставим если глобальные проблемы)
    ,RETENTION = OFF --ON=все завершенные сообщения хранятся в очереди до окончания диалога
	,POISON_MESSAGE_HANDLING (STATUS = OFF) --ON=после 5 ошибок очередь будет отключена
	,ACTIVATION (
		STATUS = ON --OFF=очередь не активирует ХП(в PROCEDURE_NAME)(ставим на время исправления ХП, но с потерей сообщений)  
		,PROCEDURE_NAME = dbo.ConfirmMessage
		,MAX_QUEUE_READERS = 1 --количество потоков(ХП одновременно вызванных) при обработке сообщений(0-32767)
							   --(0=тоже не позовется процедура)(ставим на время исправления ХП, без потери сообщений) 
		,EXECUTE AS OWNER --учетка от имени которой запустится ХП
	) 

GO
ALTER QUEUE [dbo].[TargetQueueMovies] 
	WITH STATUS = ON 
    ,RETENTION = OFF 
	,POISON_MESSAGE_HANDLING (STATUS = OFF)
	,ACTIVATION (
		STATUS = ON 
		,PROCEDURE_NAME = dbo.GetActorToMovie
		,MAX_QUEUE_READERS = 1
		,EXECUTE AS OWNER 
	) 

GO



--выполнение
--exec [dbo].[ConnectActorToMovie] @ActorId =1,@MovieId = 1,@RoleId = 1,@RoleName = 'qwww'

--очистка
--DROP SERVICE [//Movies/SB/TargetService]
--DROP SERVICE [//Movies/SB/InitiatorService]
--DROP QUEUE [dbo].[TargetQueueMovies]
--DROP QUEUE [dbo].[InitiatorQueueMovies]
--DROP CONTRACT [//Movies/SB/Contract]
--DROP MESSAGE TYPE [//Movies/SB/RequestMessage]
--DROP MESSAGE TYPE [//Movies/SB/ReplyMessage]
--DROP PROCEDURE IF EXISTS  dbo.ConnectActorToMovie;
--DROP PROCEDURE IF EXISTS  Sales.GetNewInvoice;
--DROP PROCEDURE IF EXISTS  Sales.ConfirmInvoice;