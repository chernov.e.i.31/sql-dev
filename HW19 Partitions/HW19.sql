ALTER DATABASE Movies ADD FILEGROUP [MovieYear]
GO
ALTER DATABASE [Movies] ADD FILE 
( NAME = N'MovieYear', FILENAME = N'/var/opt/mssql/data/MovieYear.ndf' , 
SIZE = 5120 , FILEGROWTH = 65536KB ) TO FILEGROUP [MovieYear]
GO

CREATE PARTITION FUNCTION [fnMovieYearPartition](int) AS RANGE RIGHT FOR VALUES
(2000, 2010, 2020, 2030, 2040);																																																									
GO

CREATE PARTITION SCHEME [schmMovieYearPartition] AS PARTITION [fnMovieYearPartition] 
ALL TO ([MovieYear])
GO
-- возьмем уже существующие данные
SELECT * INTO dbo.MoviesYear
FROM dbo.Movies;
GO

CREATE CLUSTERED INDEX [ClusteredIndex_on_schmMovieYearPartition_638468228183392108] ON [dbo].[MoviesYear]([Year])
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [schmMovieYearPartition]([Year])

-- проверим разбивку
SELECT  $PARTITION.fnMovieYearPartition([year]) AS Partition
		, COUNT(*) AS [COUNT]
FROM dbo.MoviesYear
GROUP BY $PARTITION.fnMovieYearPartition([year]) 
ORDER BY Partition ; 