/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.
Занятие "02 - Оператор SELECT и простые фильтры, JOIN".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД WideWorldImporters можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/download/wide-world-importers-v1.0/WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Все товары, в названии которых есть "urgent" или название начинается с "Animal".
Вывести: ИД товара (StockItemID), наименование товара (StockItemName).
Таблицы: Warehouse.StockItems.
*/

select StockItemID, StockItemName  
from Warehouse.StockItems
where StockItemName like '%urgent%'
	or StockItemName like 'Animal%'

/*
2. Поставщиков (Suppliers), у которых не было сделано ни одного заказа (PurchaseOrders).
Сделать через JOIN, с подзапросом задание принято не будет.
Вывести: ИД поставщика (SupplierID), наименование поставщика (SupplierName).
Таблицы: Purchasing.Suppliers, Purchasing.PurchaseOrders.
По каким колонкам делать JOIN подумайте самостоятельно.
*/
select s.SupplierID, s.SupplierName
from Purchasing.Suppliers s
	left join Purchasing.PurchaseOrders o on s.SupplierID = o.SupplierID
where o.SupplierID is null

/*
3. Заказы (Orders) с ценой товара (UnitPrice) более 100$ 
либо количеством единиц (Quantity) товара более 20 штук
и присутствующей датой комплектации всего заказа (PickingCompletedWhen).
Вывести:
* OrderID
* дату заказа (OrderDate) в формате ДД.ММ.ГГГГ
* название месяца, в котором был сделан заказ
* номер квартала, в котором был сделан заказ
* треть года, к которой относится дата заказа (каждая треть по 4 месяца)
* имя заказчика (Customer)
Добавьте вариант этого запроса с постраничной выборкой,
пропустив первую 1000 и отобразив следующие 100 записей.

Сортировка должна быть по номеру квартала, трети года, дате заказа (везде по возрастанию).

Таблицы: Sales.Orders, Sales.OrderLines, Sales.Customers.
*/
declare @pagesize int = 100, @page int = 11

select  o.OrderID, 
	o.OrderDate, 
	DATENAME(MONTH,o.OrderDate) [Month], 
	DATEPART(QUARTER, o.OrderDate) [Quarter],
	case 
		when MONTH(o.OrderDate) between 1 and 4 then 1
		when MONTH(o.OrderDate) between 5 and 8 then 2
		else 3
	end ThirdOfTheYear,
	c.CustomerName 
from Sales.Orders o
	join Sales.OrderLines l on l.OrderID = o.OrderID
	join Sales.Customers c on c.CustomerID = o.CustomerID
where l.UnitPrice >100 or ( l.Quantity>20 and o.PickingCompletedWhen is not null)
order by [Quarter], ThirdOfTheYear, o.OrderDate
offset (@page-1)* @pagesize Row fetch next @pagesize rows only;

/*
4. Заказы поставщикам (Purchasing.Suppliers),
которые должны быть исполнены (ExpectedDeliveryDate) в январе 2013 года
с доставкой "Air Freight" или "Refrigerated Air Freight" (DeliveryMethodName)
и которые исполнены (IsOrderFinalized).
Вывести:
* способ доставки (DeliveryMethodName)
* дата доставки (ExpectedDeliveryDate)
* имя поставщика
* имя контактного лица принимавшего заказ (ContactPerson)

Таблицы: Purchasing.Suppliers, Purchasing.PurchaseOrders, Application.DeliveryMethods, Application.People.
*/
select d.DeliveryMethodName, o.ExpectedDeliveryDate, s.SupplierName, p.PreferredName
from Purchasing.Suppliers s
	join Purchasing.PurchaseOrders o on o.SupplierID = s.SupplierID
		and EOMONTH(o.ExpectedDeliveryDate) = '20130131'
	join Application.DeliveryMethods d on d.DeliveryMethodID = o.DeliveryMethodID
		and d.DeliveryMethodName in ('Air Freight', 'Refrigerated Air Freight')
	join Application.People p on p.PersonID =  isnull(s.PrimaryContactPersonID,s.AlternateContactPersonID)
where o.IsOrderFinalized = 1
/*
5. Десять последних продаж (по дате продажи) с именем клиента и именем сотрудника,
который оформил заказ (SalespersonPerson).
Сделать без подзапросов.
*/

select top 10 with ties o.OrderDate, p.PreferredName ClientName, p2.PreferredName SalespersonName
from Sales.Orders o
	join Application.People p on p.PersonID =  o.CustomerID
	join Application.People p2 on p2.PersonID =  o.SalespersonPersonID
order by o.OrderDate desc

/*
6. Все ид и имена клиентов и их контактные телефоны,
которые покупали товар "Chocolate frogs 250g".
Имя товара смотреть в таблице Warehouse.StockItems.
*/

select distinct p.PersonID, p.PreferredName ClientName, p.PhoneNumber 
from Sales.Orders o
	join Sales.OrderLines l on l.OrderID = o.OrderID
	join Warehouse.StockItems s on s.StockItemID = l.StockItemID
		and StockItemName = 'Chocolate frogs 250g'
	join Application.People p on p.PersonID =  o.CustomerID

