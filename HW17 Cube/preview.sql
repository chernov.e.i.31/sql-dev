CREATE TABLE [MoviesWithTax] (
	[Id] integer not null identity(1, 1) PRIMARY KEY,
	[Name] nvarchar(100) not null,
	[Year] int not null,
	[Description] nvarchar(700),
	[CountrieName] nvarchar(100) not null,
	[Tax] decimal,
	[GenreName] nvarchar(50) not null
)

insert into MoviesWithTax([Name],[Year],Description,CountrieName,Tax,GenreName)
select m.Name, m.Year, m.Description, c.Name, p.Tax, g.Name
from Movies m
	join Premiere p on p.MovieId = m.Id
	join Countries c on c.Id =p.CountrieId
	join GenresOfMovies gm on gm.MovieId=m.Id
	join Genres g on g.Id = gm.GenreId

