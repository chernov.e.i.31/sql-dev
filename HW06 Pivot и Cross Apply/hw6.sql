/*
Домашнее задание по курсу MS SQL Server Developer в OTUS.

Занятие "05 - Операторы CROSS APPLY, PIVOT, UNPIVOT".

Задания выполняются с использованием базы данных WideWorldImporters.

Бэкап БД можно скачать отсюда:
https://github.com/Microsoft/sql-server-samples/releases/tag/wide-world-importers-v1.0
Нужен WideWorldImporters-Full.bak

Описание WideWorldImporters от Microsoft:
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-what-is
* https://docs.microsoft.com/ru-ru/sql/samples/wide-world-importers-oltp-database-catalog
*/

-- ---------------------------------------------------------------------------
-- Задание - написать выборки для получения указанных ниже данных.
-- ---------------------------------------------------------------------------

USE WideWorldImporters

/*
1. Требуется написать запрос, который в результате своего выполнения 
формирует сводку по количеству покупок в разрезе клиентов и месяцев.
В строках должны быть месяцы (дата начала месяца), в столбцах - клиенты.

Клиентов взять с ID 2-6, это все подразделение Tailspin Toys.
Имя клиента нужно поменять так чтобы осталось только уточнение.
Например, исходное значение "Tailspin Toys (Gasport, NY)" - вы выводите только "Gasport, NY".
Дата должна иметь формат dd.mm.yyyy, например, 25.12.2019.

Пример, как должны выглядеть результаты:
-------------+--------------------+--------------------+-------------+--------------+------------
InvoiceMonth | Peeples Valley, AZ | Medicine Lodge, KS | Gasport, NY | Sylvanite, MT | Jessie, ND
-------------+--------------------+--------------------+-------------+--------------+------------
01.01.2013   |      3             |        1           |      4      |      2        |     2
01.02.2013   |      7             |        3           |      4      |      2        |     1
-------------+--------------------+--------------------+-------------+--------------+------------
*/

select * 
from (
		SELECT DATEADD(month, DATEDIFF(month, 0, o.OrderDate), 0) dt,
			substring(c.CustomerName, CHARINDEX('(', c.CustomerName)+1, CHARINDEX(')', c.CustomerName)-(CHARINDEX('(', c.CustomerName)+1)) Customer,
			ol.Quantity
		FROM Sales.Orders AS o 
			join Sales.OrderLines ol ON o.OrderID = ol.OrderID
			join Sales.Customers c on c.CustomerID = o.CustomerID
		where o.CustomerID between 2 and 6
	) as ord
pivot (sum(ord.Quantity)
	for Customer in ([Sylvanite, MT],[Peeples Valley, AZ],[Medicine Lodge, KS],[Gasport, NY],[Jessie, ND]))
as pvt
order by 1

/*
2. Для всех клиентов с именем, в котором есть "Tailspin Toys"
вывести все адреса, которые есть в таблице, в одной колонке.

Пример результата:
----------------------------+--------------------
CustomerName                | AddressLine
----------------------------+--------------------
Tailspin Toys (Head Office) | Shop 38
Tailspin Toys (Head Office) | 1877 Mittal Road
Tailspin Toys (Head Office) | PO Box 8975
Tailspin Toys (Head Office) | Ribeiroville
----------------------------+--------------------
*/
SELECT CustomerName, Address
FROM (
	select CustomerName, DeliveryAddressLine1, DeliveryAddressLine2 , PostalAddressLine1, PostalAddressLine2
	from Sales.Customers
	) AS Cust
UNPIVOT (Address FOR Name IN (DeliveryAddressLine1, DeliveryAddressLine2 , PostalAddressLine1, PostalAddressLine2)) AS unpt;

/*
3. В таблице стран (Application.Countries) есть поля с цифровым кодом страны и с буквенным.
Сделайте выборку ИД страны, названия и ее кода так, 
чтобы в поле с кодом был либо цифровой либо буквенный код.

Пример результата:
--------------------------------
CountryId | CountryName | Code
----------+-------------+-------
1         | Afghanistan | AFG
1         | Afghanistan | 4
3         | Albania     | ALB
3         | Albania     | 8
----------+-------------+-------
*/
create table #tbl (CountryID INT,CountryName VARCHAR(250),IsoAlpha3Code VARCHAR(250), IsoNumericCode VARCHAR(250))

insert #tbl
select CountryID, CountryName, IsoAlpha3Code, CAST(IsoNumericCode as VARCHAR(10)) as IsoNumericCode
	from Application.Countries
	
SELECT CountryID ,CountryName, Code
FROM (
		select * from #tbl
	) as Countries
unpivot (Code for codes in (IsoAlpha3Code,IsoNumericCode ) ) as unpt

/*
4. Выберите по каждому клиенту два самых дорогих товара, которые он покупал.
В результатах должно быть ид клиета, его название, ид товара, цена, дата покупки.
*/

select c.CustomerID, c.CustomerName, ord.*
from Sales.Customers c
	cross apply (
		select top 2 ol.StockItemID, ol.UnitPrice, max(o.OrderDate) OrderDate
		from Sales.Orders o 
			join Sales.OrderLines ol on ol.OrderID = o.OrderID
		where o.CustomerID = c.CustomerID
		group by ol.StockItemID, ol.UnitPrice
		order by ol.UnitPrice desc
	) ord

