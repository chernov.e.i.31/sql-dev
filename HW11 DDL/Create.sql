CREATE DATABASE [Movies]
ON  PRIMARY 
( NAME = movies, FILENAME = N'/var/opt/mssql/data/Movies.mdf' , 
	SIZE = 8MB , 
	MAXSIZE = 5GB, 
	FILEGROWTH = 65536KB )
 LOG ON 
( NAME = movies_log, FILENAME = N'/var/opt/mssql/data/Movies_log.ldf' , 
	SIZE = 8MB , 
	MAXSIZE = 1GB , 
	FILEGROWTH = 65536KB )
GO

use [Movies];

-- Таблицы 
CREATE TABLE [Movies] (
  [Id] integer not null identity(1, 1) PRIMARY KEY,
  [Name] nvarchar(100) not null,
  [Year] int not null,
  [Budget] decimal,
  [Description] nvarchar(700)
)
GO

CREATE TABLE [Persons] (
  [Id] integer not null identity(1, 1) PRIMARY KEY,
  [Name] nvarchar(100) not null,
  [DoB] datetime
)
GO

CREATE TABLE [Countries] (
  [Id] integer not null identity(1, 1) PRIMARY KEY,
  [Name] nvarchar(100) not null
)
GO

CREATE TABLE [Genres] (
  [Id] integer not null identity(1, 1) PRIMARY KEY,
  [Name] nvarchar(50) not null
)
GO

CREATE TABLE [Roles] (
  [Id] integer not null identity(1, 1) PRIMARY KEY,
  [Name] nvarchar(100) not null
)
GO

CREATE TABLE [Premiere] (
  [MovieId] integer not null,
  [CountrieId] integer not null,
  [PremiereDate] date,
  [Tax] decimal,
  [LocalName] nvarchar(100) not null,
  CONSTRAINT PK_Premiere_MovieIdCountrieId PRIMARY KEY CLUSTERED ([MovieId],[CountrieId])
)
GO

CREATE TABLE [CountriesOfOrigin] (
  [MovieId] integer not null,
  [CountrieId] integer not null,
  CONSTRAINT PK_CountriesOfOrigin_MovieIdCountrieId PRIMARY KEY CLUSTERED ([MovieId],[CountrieId])
)
GO

CREATE TABLE [Actors] (
  [MovieId] integer not null,
  [PersonId] integer not null,
  [RoleId] integer not null,
  [RoleName] nvarchar(100) not null
)
GO

CREATE TABLE [GenresOfMovies] (
  [MovieId] integer not null,
  [GenreId] integer not null,
  CONSTRAINT PK_GenresOfMovies_MovieIdGenreId PRIMARY KEY CLUSTERED ([MovieId],[GenreId])
)
GO

-- внешние ключи
ALTER TABLE [Premiere] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO

ALTER TABLE [Premiere] ADD FOREIGN KEY ([CountrieId]) REFERENCES [Countries] ([Id])
GO

ALTER TABLE [CountriesOfOrigin] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO

ALTER TABLE [CountriesOfOrigin] ADD FOREIGN KEY ([CountrieId]) REFERENCES [Countries] ([Id])
GO

ALTER TABLE [Actors] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO

ALTER TABLE [Actors] ADD FOREIGN KEY ([PersonId]) REFERENCES [Persons] ([Id])
GO

ALTER TABLE [Actors] ADD FOREIGN KEY ([RoleId]) REFERENCES [Roles] ([Id])
GO

ALTER TABLE [GenresOfMovies] ADD FOREIGN KEY ([GenreId]) REFERENCES [Genres] ([Id])
GO

ALTER TABLE [GenresOfMovies] ADD FOREIGN KEY ([MovieId]) REFERENCES [Movies] ([Id])
GO

-- индексы

create index idx_year on [Movies]([Year]);
create index idx_name on [Countries]([Name]);


-- ограничения

ALTER TABLE [Movies] ADD CONSTRAINT YearCheck CHECK ([Year] >=1895);
ALTER TABLE [Persons] ADD CONSTRAINT DoBCheck CHECK ([DoB] >= '18000101');
ALTER TABLE [Persons] ADD CONSTRAINT PersonNameCheck CHECK ([Name] != '');
ALTER TABLE [Countries] ADD CONSTRAINT CountrieNameCheck CHECK ([Name] != '');
ALTER TABLE [Genres] ADD CONSTRAINT GenreNameCheck CHECK ([Name] != '');
ALTER TABLE [Roles] ADD CONSTRAINT RolesNameCheck CHECK ([Name] != '');
ALTER TABLE [Premiere] ADD CONSTRAINT TaxCheck CHECK ([Tax] > 0);
ALTER TABLE [Actors] ADD CONSTRAINT RoleNameCheck CHECK ([RoleName] != '');


